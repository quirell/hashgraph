package hashgraph;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class HashGraph {

    @Autowired
    private EventService service;

    private int membersCount;
    private int coinFrequency;
    private Map<Hash, Event> events;
    private Event top;

    private void divideRounds() {

    }

    private void decideFame() {

    }

    private void findOrder() {

    }

    private void sync(long id, Object payload) {
        service.send(createEvent(id,payload));
    }

    private Event createEvent(long id, Object payload) {
        return new Event();
    }
}
