package hashgraph;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("event")
@RestController
public class EventController {

    @RequestMapping(method = RequestMethod.POST)
    public Event receiveEvent(Event input) {
        return input;
    }
}
