package hashgraph;


import java.time.LocalDateTime;

public class Event {
    private long id;
    private Object data;
    private Hash selfHash;
    private Hash otherHash;
    private LocalDateTime time;

    boolean witness;
    boolean famous;
    boolean consensus;
    long generation;
    long round;


}
